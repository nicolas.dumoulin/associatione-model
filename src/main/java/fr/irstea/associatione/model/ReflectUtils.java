/*
 * Copyright (C) 2012 dumoulin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.associatione.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dumoulin
 */
public class ReflectUtils {

    private static final Logger LOGGER = Logger.getLogger(ReflectUtils.class.getName());

    /**
     *
     * @param type
     * @param name
     * @return null if the method wasn't found
     */
    public static Method getMethod(Class type, String name) throws NoSuchFieldException {
        Class current = type;
        while (current != null) {
            for (Method m : current.getDeclaredMethods()) {
                if (m.getName().equals(name)) {
                    return m;
                }
            }
            current = current.getSuperclass();
        }
        throw new NoSuchFieldException("The method " + name + " hasn't been found in the class " + type.getName());
    }

    /**
     *
     * @param method
     * @param instance
     * @return null if a problem has occured during the introspection.
     */
    public static Object invokeMethod(Method method, Object instance) {
        boolean old = method.isAccessible();
        method.setAccessible(true);
        Object value = null;
        try {
            boolean accessible = method.isAccessible();
            method.setAccessible(true);
            value = method.invoke(instance);
            method.setAccessible(accessible);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOGGER.log(Level.SEVERE, "Error during method invocation " + method, ex);
        }
        method.setAccessible(old);
        return value;
    }

    /**
     *
     * @param methodname
     * @param instance
     * @return null if a problem has occured during the introspection.
     */
    public static Object invokeMethod(String methodname, Object instance) throws NoSuchFieldException {
        Object value = null;
        Method method = getMethod(instance.getClass(), methodname);
        if (method != null) {
            value = ReflectUtils.invokeMethod(method, instance);
        }
        return value;
    }

    /**
     *
     * @param instance
     * @param path The path of the method to retrieve from the class of the
     * instance given. The path must contain a dot "." for accessing nested
     * methods. The element of a list can be accessed by its index. For example
     * : <tt>elements.2.aMethod</tt>
     * @return
     * @throws NoSuchFieldException
     */
    public static MethodOnInstance getMethodOnInstance(Object instance, String path) throws NoSuchFieldException {
        if (path.contains(".")) {
            String[] nodes = path.split("\\.");
            for (int i = 0; i < nodes.length - 1; i++) {
                if (nodes[i].matches("[0-9]*")) {
                    instance = ((List) instance).get(Integer.parseInt(nodes[i]));
                } else {
                    instance = ReflectUtils.invokeMethod(nodes[i], instance);
                }
            }
            path = nodes[nodes.length - 1];
        }
        return new MethodOnInstance(instance, getMethod(instance.getClass(), path));
    }

    public static void invokeMethodFromPath(Object instance, String path) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        getMethodOnInstance(instance, path).invoke();
    }

    /**
     * Class that wraps a method with an instance that can be invoked.
     */
    public static class MethodOnInstance {

        private final Object instance;
        private final Method method;

        public MethodOnInstance(Object instance, Method field) {
            this.instance = instance;
            this.method = field;
        }

        public Method getMethod() {
            return method;
        }

        public Object getInstance() {
            return instance;
        }

        public Object invoke() throws IllegalAccessException {
            return ReflectUtils.invokeMethod(method, instance);
        }
    }
}
