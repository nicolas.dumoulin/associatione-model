/*
 * Copyright (C) 2016 Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.associatione.model.distorsion;

import fr.irstea.associatione.model.Individual;
import fr.irstea.associatione.model.Model;
import fr.irstea.associatione.model.Parameter;
import fr.irstea.associatione.model.Parameters;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class DistorsionFromOpinions implements Distorsion, Parameter {

    private Parameters parameters;

    @Override
    public void init(Model model) {
        this.parameters = model.getParameters();
    }

    @Override
    public double compute(Individual from, Individual to) {
        double sum = 0;
        for (int k = 0; k < parameters.getNbSubject(); k++) {
            sum += Math.abs(from.getOpinionOnSubject(k) - to.getOpinionOnSubject(k));
        }
        return sum / parameters.getNbSubject();
    }

}
