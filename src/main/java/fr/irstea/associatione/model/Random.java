/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.associatione.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import umontreal.iro.lecuyer.rng.MRG32k3a;
import umontreal.iro.lecuyer.rng.RandomPermutation;

/**
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 * @author sylvie.huet
 */
public class Random {

    private MRG32k3a randomStream;

    /**
     * This method returns an integer picked out following a Uniform law between
     * i and i1. The values i and i1 are included (can be picked out)
     */
    public int nextInt(int i, int i1) {
        return randomStream.nextInt(i, i1);
    }

    /**
     * This method returns a double picked out following a Uniform law between 0
     * and 1 with 0 and 1 excluded (can't be picked out)
     */
    public double nextDouble() {
        return randomStream.nextDouble();
    }

    /**
     * This method return a random order for a sequence of leng numbers.
     */
    public int[] randomList(int leng) {
        int[] vec = new int[leng];
        for (int i = 0; i < leng; i++) {
            vec[i] = i;
        }
        RandomPermutation.shuffle(vec, randomStream);
        return vec;
    }

    public String getSeedToString() {
        StringBuilder buff = new StringBuilder();
        for (long val : randomStream.getState()) {
            buff.append(val);
            buff.append("   ");
        }
        return buff.toString();
        // this code make troubles in exception stacktrace formatting (don't know why…)
        //return Arrays.toString(randomStream.getState());
    }

    /**
     * This method should only be called by the simulation starter. The initial
     * status for the RNG is initialized by the simulator at startup.
     *
     * @param seed
     */
    public void setSeed(long[] seed) throws ProcessingException {
        if (randomStream != null) {
            throw new ProcessingException("Trying to reinit the seed of the RNG although it is already initialized!");
        }
        randomStream = new MRG32k3a();
        randomStream.setSeed(seed);
    }

    /**
     * This method should only be called by the simulation starter.
     * Reinitializes the stream to the beginning of its next substream (see SSJ
     * documentation)
     *
     * @param count nb of substreams to shift. Give 0 if you want the default
     * one.
     */
    public void resetNextSubstream(int count) throws ProcessingException {
        if (randomStream != null) {
            throw new ProcessingException("Trying to reinit the seed of the RNG although it is already initialized!");
        }
        randomStream = new MRG32k3a();
        randomStream.setSeed(new long[]{12345, 12345, 12345, 12345, 12345, 12345});
        for (int i = 0; i < count; i++) {
            randomStream.resetNextSubstream();
        }
    }

    /**
     * Randomly permutes list. This method permutes the whole list.
     *
     * @param list the list to process
     */
    public void shuffle(List list) {
        RandomPermutation.shuffle(list, randomStream);
    }

    /**
     * Picks randomly some elements from a list.
     *
     * @param <E>
     * @param list
     * @param nb the number of elements to pick out
     * @return null, if nb <= 0
     */
    public <E> List<E> pickElements(List<E> list, int nb) {
        if (nb <= 0) {
            return null;
        }
        List<E> result = new ArrayList<E>(nb);
        List<E> copy = new ArrayList<E>(list);
        for (int i = 0; i < nb; i++) {
            int index = nextInt(0, copy.size() - 1);
            result.add(copy.remove(index));
        }
        return result;
    }

    /**
     * Build an array of indexes for randomly picking elements in a matrix.
     *
     * @param elements
     * @return an array of two-dimensional indexes
     */
    public List<int[]> buildRandomMatrixIndexes(int size) {
        List<int[]> indexes = new ArrayList<int[]>();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                indexes.add(new int[]{i, j});
            }
        }
        shuffle(indexes);
        return indexes;
    }

}
