/*
 * Copyright (C) 2016 Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.associatione.model;

import fr.irstea.associatione.model.distorsion.Distorsion;
import java.lang.reflect.Field;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class Parameters {

    private int seedIndex;
    private int popSize;
    private int nbSubject;
    private Distorsion distorsion;
    private double rho;
    private double sigma;
    private transient Random random;

    public Parameters(int seedIndex, int popSize, int nbSubject, Distorsion distorsion, double rho, double sigma) {
        this.seedIndex = seedIndex;
        this.popSize = popSize;
        this.nbSubject = nbSubject;
        this.distorsion = distorsion;
        this.rho = rho;
        this.sigma = sigma;
    }
    
    public void init(Model model) throws IllegalArgumentException, IllegalAccessException {
        this.random = model.getRandom();
        for (Field field : Parameters.class.getDeclaredFields()) {
            if (Parameter.class.isAssignableFrom(field.get(this).getClass())) {
                ((Parameter)field.get(this)).init(model);
            }
        }
    }

    public Random getRandom() {
        return random;
    }

    
    public int getSeedIndex() {
        return seedIndex;
    }
    
    public int getPopSize() {
        return popSize;
    }

    public int getNbSubject() {
        return nbSubject;
    }

    public Distorsion getDistorsion() {
        return distorsion;
    }

    public double getRho() {
        return rho;
    }

    public double getSigma() {
        return sigma;
    }
    
    
    
}
