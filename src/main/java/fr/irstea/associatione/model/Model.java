/*
 * Copyright (C) 2016 Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.associatione.model;

import fr.irstea.associatione.model.distorsion.DistorsionFromOpinions;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class Model {

    private final Parameters parameters;
    private final Random random;
    private final Individual[] individuals;
    private final List<Individual[]> couples;
    private int timestep;

    public Model(Parameters parameters) throws ProcessingException {
        this.parameters = parameters;
        this.random = new Random();
        random.resetNextSubstream(parameters.getSeedIndex());
        this.individuals = new Individual[parameters.getPopSize()];
        for (int i = 0; i < individuals.length; i++) {
            double[] individualsOpinions = new double[parameters.getPopSize()];
            for (int j = 0; j < individualsOpinions.length; j++) {
                individualsOpinions[j] = 0;
            }
            double[] subjectOpinions = new double[parameters.getNbSubject()];
            for (int j = 0; j < subjectOpinions.length; j++) {
                subjectOpinions[j] = random.nextDouble() * 2 - 1;
            }
            individuals[i] = new Individual(i, parameters.getPopSize(), individualsOpinions, subjectOpinions);
        }
        couples = new ArrayList<>();
        for (int i = 0; i < individuals.length; i++) {
            for (int j = 0; j < i; j++) {
                couples.add(new Individual[]{individuals[i], individuals[j]});
            }
        }
        timestep = 0;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public int getTimestep() {
        return timestep;
    }

    public Random getRandom() {
        return random;
    }
    
    public void iter() {
        List<Individual[]> couplesShuffled = new ArrayList<>(couples);
        random.shuffle(couplesShuffled);
        for (Individual[] couple : couplesShuffled) {
            iterMeeting(couple[0], couple[1]);
        }
        timestep++;
    }

    private void iterMeeting(Individual ind1, Individual ind2) {
        ind1.changeOpinion(parameters, ind2);
        ind2.changeOpinion(parameters, ind1);
        ind1.computeInfluences(parameters);
        ind2.computeInfluences(parameters);
        ind1.argumentationImpact(parameters, ind2);
        ind2.argumentationImpact(parameters, ind1);
    }

    public double[] getCredibility() {
        double[] data = new double[individuals.length * (individuals.length - 1)];
        int index = 0;
        for (int i = 0; i < individuals.length; i++) {
            Individual ind = individuals[i];
            for (int j = 0; j < individuals.length; j++) {
                if (i != j) {
                    data[index] = ind.getInfluence(j);
                    index++;
                }
            }
        }
        return data;
    }

    public double[] getOpinionsOnAgents() {
        double[] data = new double[individuals.length * (individuals.length - 1)];
        int index = 0;
        for (int i = 0; i < individuals.length; i++) {
            Individual ind = individuals[i];
            for (int j = 0; j < individuals.length; j++) {
                if (i != j) {
                    data[index] = ind.getOpinionOnIndividual(j);
                    index++;
                }
            }
        }
        return data;
    }

    public double[] getOpinionsMeanOnSubjects() {
        double[] data = new double[individuals.length];
        for (int i = 0; i < individuals.length; i++) {
            Individual individual = individuals[i];
            data[i] = 0;
            for (int k = 0; k < parameters.getNbSubject(); k++) {
                data[i] += individual.getOpinionOnSubject(k);
            }
            data[i] = data[i] / parameters.getNbSubject();
        }
        return data;
    }

    public double[] getSubjectiveNorm() {
        double[] opinionsMeanOnSubjects = getOpinionsMeanOnSubjects();
        int popSize = parameters.getPopSize();
        double[] data = new double[popSize];
        for (int i = 0; i < popSize; i++) {
            data[i] = 0;
            double sumInfluences = 0;
            for (int j = 0; j < popSize; j++) {
                if (i != j) {
                    sumInfluences += individuals[i].getInfluence(j);
                    data[i] += opinionsMeanOnSubjects[j] * individuals[i].getInfluence(j);
                }
            }
            data[i] = data[i] / (sumInfluences);// * (popSize - 1));
            if (sumInfluences == 0) {
                data[i] = 0;
            }
        }
        return data;
    }

    public static void main(String[] args) throws ProcessingException, IllegalArgumentException, IllegalAccessException {
        Parameters parameters = new Parameters(0, 500, 1, new DistorsionFromOpinions(), 1, 0.3);
        Model model = new Model(parameters);
        parameters.init(model);
        for (int i = 0; i < 500; i++) {
            System.out.println("Iteration " + i);
            model.iter();
        }
    }
}
