/*
 * Copyright (C) 2016 Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.associatione.model;

import java.util.Arrays;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class Individual {

    private final int id;
    private final double[] individualsOpinions;
    private final double[] subjectOpinions;
    private final double[] influences;

    public Individual(int id, int popSize, int nbSubject) {
        this.id = id;
        individualsOpinions = new double[popSize];
        subjectOpinions = new double[popSize];
        influences = new double[popSize];
        Arrays.fill(individualsOpinions, 0);
        Arrays.fill(subjectOpinions, 0);
        Arrays.fill(influences, 0);
    }

    public Individual(int id, int popSize, double[] individualsOpinions, double[] subjectOpinions) {
        this.id = id;
        this.individualsOpinions = individualsOpinions;
        this.subjectOpinions = subjectOpinions;
        influences = new double[popSize];
        Arrays.fill(influences, 0);
    }

    public double getOpinionOnIndividual(int ind) {
        return individualsOpinions[ind];
    }

    public double getOpinionOnSubject(int subject) {
        return subjectOpinions[subject];
    }

    public double getInfluence(int ind) {
        return influences[ind];
    }

    public void computeInfluences(Parameters parameters) {
        for (int fromInd = 0; fromInd < individualsOpinions.length; fromInd++) {
            influences[fromInd] = 1 / (1 + Math.exp(-(individualsOpinions[fromInd] - individualsOpinions[id]) / parameters.getSigma()));
        }
    }

    public void changeOpinion(Parameters parameters, Individual fromInd) {
        double distort1 = parameters.getDistorsion().compute(this, fromInd);
        // noise
        double distort2 = parameters.getRandom().nextDouble() * 2 * distort1 - distort1;
        individualsOpinions[id] += influences[fromInd.id] * parameters.getRho() * (fromInd.individualsOpinions[id] - individualsOpinions[id] + distort2);
        distort2 = parameters.getRandom().nextDouble() * 2 * distort1 - distort1;
        individualsOpinions[fromInd.id] += influences[fromInd.id] * parameters.getRho() * (fromInd.individualsOpinions[fromInd.id] - individualsOpinions[fromInd.id] + distort2);
    }

    public void argumentationImpact(Parameters parameters, Individual fromInd) {
        for (int k = 0; k < parameters.getNbSubject(); k++) {
            subjectOpinions[k] += influences[fromInd.id] * parameters.getRho() * (fromInd.subjectOpinions[k] - subjectOpinions[k]);
        }
    }

}
