/*
 * Copyright (C) 2016 Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.associatione.model.gui;

import fr.irstea.associatione.model.Model;
import fr.irstea.associatione.model.ProcessingException;
import fr.irstea.associatione.model.ReflectUtils;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class TimeSerieChart {

    private JPanel display;
    private List<Timeserie> timeseries;

    public static class Timeserie {

        private final String name;
        private final boolean histogram, series;
        private final ReflectUtils.MethodOnInstance methodOnInstance;
        private final List<double[]> timeserie;
        private ChartPanel histogramPanel;
        private final XYSeriesCollection xySeries;

        public Timeserie(Model model, String fetcherPath, String name, boolean histogram, boolean series) throws ProcessingException {
            this.name = name;
            this.histogram = histogram;
            this.series = series;
            try {
                this.methodOnInstance = ReflectUtils.getMethodOnInstance(model, fetcherPath);
                this.timeserie = new ArrayList<>();
                xySeries = new XYSeriesCollection();
                histogramPanel = new ChartPanel(createHistogram(name, 0, (double[]) methodOnInstance.invoke()));
            } catch (NoSuchFieldException | IllegalAccessException ex) {
                throw new ProcessingException("Error during method retrieval", ex);
            }
        }

        private JPanel getHistogram() {
            return histogramPanel;
        }

        private JPanel getSeries() {
            return new ChartPanel(ChartFactory.createXYLineChart(name, "timesteps", name, xySeries, PlotOrientation.VERTICAL, false, true, false));
        }

        public void add(double[] data) {
            timeserie.add(data);
        }

        public void update(int timestep) throws IllegalAccessException {
            final double[] data = (double[]) methodOnInstance.invoke();
            histogramPanel.setChart(createHistogram(name, timestep, data));
            timeserie.add(data);
            if (xySeries.getSeriesCount() == 0) {
                for (int i = 0; i < data.length; i++) {
                    xySeries.addSeries(new XYSeries(i));
                }
            }
            for (int i = 0; i < data.length; i++) {
                xySeries.getSeries(i).add(timeserie.size() - 1, data[i]);
            }
        }

    }

    public TimeSerieChart() {
        display = new JPanel(new GridLayout(0, 2));
        timeseries = new ArrayList<>();
    }

    public JPanel getDisplay() {
        return display;
    }

    public void addTimeSerie(Model model, String fetcherPath, String name, boolean histogram, boolean series) throws ProcessingException {
        final Timeserie timeserie = new Timeserie(model, fetcherPath, name, histogram, series);
        timeseries.add(timeserie);
        if (histogram) {
            display.add(timeserie.getHistogram());
        }
        if (series) {
            display.add(timeserie.getSeries());
        }
    }

    public void updateCharts(int timestep) {
        for (Timeserie timeserie : timeseries) {
            try {
                timeserie.update(timestep);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(TimeSerieChart.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public static JFreeChart createHistogram(String title, int timestep, double[] data) {
        HistogramDataset dataset = new HistogramDataset();
        dataset.setType(HistogramType.RELATIVE_FREQUENCY);
        dataset.addSeries(title, data, 30);
        String plotTitle = title + " " + timestep;
        String xaxis = "opinions";
        String yaxis = "amount";
        PlotOrientation orientation = PlotOrientation.VERTICAL;
        boolean show = false;
        boolean toolTips = true;
        boolean urls = false;
        JFreeChart chart = ChartFactory.createHistogram(plotTitle, xaxis, yaxis,
                dataset, orientation, show, toolTips, urls);
        XYPlot plot = (XYPlot) chart.getPlot();
        NumberAxis numberaxis = (NumberAxis)plot.getRangeAxis();
        numberaxis.setUpperBound(1.0);
        // raw style
        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setBarPainter(new StandardXYBarPainter());
        renderer.setShadowVisible(false);
        return chart;
    }

}
