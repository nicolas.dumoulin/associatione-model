/*
 * Copyright (C) 2016 Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.associatione.model.gui;

import fr.irstea.associatione.model.Model;
import fr.irstea.associatione.model.Parameters;
import fr.irstea.associatione.model.ProcessingException;
import fr.irstea.associatione.model.distorsion.DistorsionFromOpinions;
import javax.swing.JFrame;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class GUI {

    private final JFrame frame;
    private final TimeSerieChart timeSerieChart;
    private Model model;

    public GUI() {
        frame = new JFrame("test");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        timeSerieChart = new TimeSerieChart();
        frame.setContentPane(timeSerieChart.getDisplay());
    }

    public void initCharts() throws ProcessingException {
        boolean bigPop = model.getParameters().getPopSize() > 10;
        timeSerieChart.addTimeSerie(model, "getOpinionsMeanOnSubjects", "Opinions", bigPop, true);
        timeSerieChart.addTimeSerie(model, "getCredibility", "Credibility", bigPop, !bigPop);
        timeSerieChart.addTimeSerie(model, "getOpinionsOnAgents", "Op. on agents", bigPop, true);
        timeSerieChart.addTimeSerie(model, "getSubjectiveNorm", "Subjective norm", bigPop, true);
    }

    public void init(Parameters parameters) throws ProcessingException, IllegalArgumentException, IllegalAccessException {
        model = new Model(parameters);
        parameters.init(model);
        initCharts();
        frame.pack();
        frame.setVisible(true);
        timeSerieChart.updateCharts(0);
        for (int i = 0; i < 30; i++) {
            model.iter();
            timeSerieChart.updateCharts(i + 1);
        }
    }

    public static void main(String[] args) throws Exception {
        GUI gui = new GUI();
        gui.init(new Parameters(0, 10, 1, new DistorsionFromOpinions(), 0.1, 0.05));
    }
}
